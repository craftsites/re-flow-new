$(function () {
    $('a[href*="#"]:not([href="#"])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                setTimeout(function () {
                    $('html, body').animate({
                        scrollTop: target.offset().top
                    }, 400);
                }, 0);
                return false;
            }
        }
    });
});

$(window).ready(function () {

    $('#burger').click(function () {
        $(this).toggleClass('open');
        $(this).addClass('close');
        $('html').toggleClass('menu-open');
    });

    $('#mainMenu li a').click(function () {
        $('#burger').toggleClass('open');
        $('#burger').addClass('close');
        $('html').toggleClass('menu-open');
    });

    $('.infographic-con .img-con .info').click(function () {
        $(this).toggleClass('open');
    });

    $(window).on('resize orientationChange', function (event) {
        var windowW = $(window).width();

        if (windowW <= 975) {
            $("#site-logo").appendTo(".menu-top-outer .wrapper");
        } else {
            $("#site-logo").appendTo(".menu-bottom-outer .wrapper");
        }
    });

    if ($('#section4').length) {
        $(window).scroll(function () {
            var underHero = $('.under-hero').height();
            $('.under-hero-outer').height(underHero);

            var sectionHeight = $('#section4').position().top;
            var scrollTop = $(window).scrollTop();
            var screenHeight = $(window).height();

            var totalScroll = (sectionHeight - (scrollTop + screenHeight)) / 2;

            $(".red-slide-img-con .top-img-con .img2").css("transform", "translateX(" + totalScroll + "%)");
        });
    }
});

$(window).on('load', function () {
//	setTimeout(function(){
//		$('.lazy-loader').addClass('loaded');
//	}, 0);

    setTimeout(function () {
        $('.slick').addClass('loaded');
        $(window).trigger('resize');

        setTimeout(function () {
            $('.slick').addClass('no-overflow');
            //$('.lazy-loader').addClass('loaded');
            $(window).trigger('resize');

            var url = window.location.href;
            if (url.indexOf("#") >= 0) {
                var value = url.substring(url.lastIndexOf('#'));

                $('html, body').animate({
                    scrollTop: $(value).offset().top
                }, 0);
            }

            setTimeout(function () {
                $(window).trigger('resize');
            }, 2000);

        }, 600);

        setTimeout(function () {
            $(window).trigger('resize');
        }, 1200);
    }, 20);


//	$.scrollify({
//		section : ".scroll-point",
//		setHeights: false,
//		overflowScroll: false,
//	});

    $('#nav').onePageNav({
        currentClass: 'current',
        changeHash: false,
        scrollSpeed: 750,
        scrollThreshold: 0.5,
        filter: '',
    });

    $('.quote-slide').slick({
        dots: true,
        arrows: true,
        infinite: false,
        fade: true,
    });

    $('.mob-slide').slick({
        dots: false,
        arrows: true,
        infinite: false,
        mobileFirst: true,
        responsive: [
            {
                breakpoint: 450,
                settings: "unslick"
            }
        ]
    });

    $(window).on('resize orientationChange', function (event) {
        var windowW1 = $(window).width();

        var windowS1 = true;

        if (windowW1 > 450) {
            windowS1 = true;
        } else {
            windowS1 = false;
        }

        if (windowS1 == false) {
            $('.mob-slide').slick('reinit');
        }
    });

    $('.carousel-slide').slick({
        dots: true,
        arrows: true,
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 975,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 450,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    $('.video-list').slick({
        dots: false,
        arrows: true,
        infinite: false,
        mobileFirst: true,
        responsive: [
            {
                breakpoint: 450,
                settings: "unslick"
            }
        ]
    });

    $(window).on('resize orientationChange', function (event) {
        var windowW2 = $(window).width();

        var windowS2 = true;

        if (windowW2 > 450) {
            windowS2 = true;
        } else {
            windowS2 = false;
        }

        if (windowS2 == false) {
            $('.video-list').slick('reinit');
        }
    });

    $('.news-list').slick({
        dots: false,
        arrows: true,
        infinite: false,
        mobileFirst: true,
        responsive: [
            {
                breakpoint: 450,
                settings: "unslick"
            }
        ]
    });

    $(window).on('resize orientationChange', function (event) {
        var windowW3 = $(window).width();

        var windowS3 = true;

        if (windowW3 > 450) {
            windowS3 = true;
        } else {
            windowS3 = false;
        }

        if (windowS3 == false) {
            $('.news-list').slick('reinit');
        }
    });

    $('.logo-list').slick({
        dots: false,
        arrows: true,
        infinite: false,
        mobileFirst: true,
        responsive: [
            {
                breakpoint: 450,
                settings: "unslick"
            }
        ]
    });

    $(window).on('resize orientationChange', function (event) {
        var windowW4 = $(window).width();

        var windowS4 = true;

        if (windowW4 > 450) {
            windowS4 = true;
        } else {
            windowS4 = false;
        }

        if (windowS4 == false) {
            $('.logo-list').slick('reinit');
        }
    });

    $('.fancybox').fancybox();

    $('.slider-for .video-con').fitVids();

    $('.matchHeight').matchHeight(true);

    $('.mob-slide .img-con').matchHeight(true);

    $('.carousel-slide .img-con').matchHeight(true);
});
