<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <title>API Reference</title>

  <style>
    .highlight table td { padding: 5px; }
    .highlight table pre { margin: 0; }
    .highlight .gh {
      color: #999999;
    }
    .highlight .sr {
      color: #f6aa11;
    }
    .highlight .go {
      color: #888888;
    }
    .highlight .gp {
      color: #555555;
    }
    .highlight .gs {
    }
    .highlight .gu {
      color: #aaaaaa;
    }
    .highlight .nb {
      color: #f6aa11;
    }
    .highlight .cm {
      color: #75715e;
    }
    .highlight .cp {
      color: #75715e;
    }
    .highlight .c1 {
      color: #75715e;
    }
    .highlight .cs {
      color: #75715e;
    }
    .highlight .c, .highlight .cd {
      color: #75715e;
    }
    .highlight .err {
      color: #960050;
    }
    .highlight .gr {
      color: #960050;
    }
    .highlight .gt {
      color: #960050;
    }
    .highlight .gd {
      color: #49483e;
    }
    .highlight .gi {
      color: #49483e;
    }
    .highlight .ge {
      color: #49483e;
    }
    .highlight .kc {
      color: #66d9ef;
    }
    .highlight .kd {
      color: #66d9ef;
    }
    .highlight .kr {
      color: #66d9ef;
    }
    .highlight .no {
      color: #66d9ef;
    }
    .highlight .kt {
      color: #66d9ef;
    }
    .highlight .mf {
      color: #ae81ff;
    }
    .highlight .mh {
      color: #ae81ff;
    }
    .highlight .il {
      color: #ae81ff;
    }
    .highlight .mi {
      color: #ae81ff;
    }
    .highlight .mo {
      color: #ae81ff;
    }
    .highlight .m, .highlight .mb, .highlight .mx {
      color: #ae81ff;
    }
    .highlight .sc {
      color: #ae81ff;
    }
    .highlight .se {
      color: #ae81ff;
    }
    .highlight .ss {
      color: #ae81ff;
    }
    .highlight .sd {
      color: #e6db74;
    }
    .highlight .s2 {
      color: #e6db74;
    }
    .highlight .sb {
      color: #e6db74;
    }
    .highlight .sh {
      color: #e6db74;
    }
    .highlight .si {
      color: #e6db74;
    }
    .highlight .sx {
      color: #e6db74;
    }
    .highlight .s1 {
      color: #e6db74;
    }
    .highlight .s {
      color: #e6db74;
    }
    .highlight .na {
      color: #a6e22e;
    }
    .highlight .nc {
      color: #a6e22e;
    }
    .highlight .nd {
      color: #a6e22e;
    }
    .highlight .ne {
      color: #a6e22e;
    }
    .highlight .nf {
      color: #a6e22e;
    }
    .highlight .vc {
      color: #ffffff;
    }
    .highlight .nn {
      color: #ffffff;
    }
    .highlight .nl {
      color: #ffffff;
    }
    .highlight .ni {
      color: #ffffff;
    }
    .highlight .bp {
      color: #ffffff;
    }
    .highlight .vg {
      color: #ffffff;
    }
    .highlight .vi {
      color: #ffffff;
    }
    .highlight .nv {
      color: #ffffff;
    }
    .highlight .w {
      color: #ffffff;
    }
    .highlight {
      color: #ffffff;
    }
    .highlight .n, .highlight .py, .highlight .nx {
      color: #ffffff;
    }
    .highlight .ow {
      color: #f92672;
    }
    .highlight .nt {
      color: #f92672;
    }
    .highlight .k, .highlight .kv {
      color: #f92672;
    }
    .highlight .kn {
      color: #f92672;
    }
    .highlight .kp {
      color: #f92672;
    }
    .highlight .o {
      color: #f92672;
    }
  </style>
  <link href="stylesheets/screen.css" rel="stylesheet" media="screen" />
  <link href="stylesheets/print.css" rel="stylesheet" media="print" />
  <script src="javascripts/all.js"></script>
</head>

<body class="index" data-languages="[]">
<?php

if(isset($_POST['access']) && $_POST['access'] == 'apidemodocs66!') { ?>

  <a href="#" id="nav-button">
      <span>
        NAV
        <img src="images/navbar.png" alt="Navbar" />
      </span>
  </a>
  <div class="tocify-wrapper">
    <img src="images/logo.png" alt="Logo" />
    <div class="search">
      <input type="text" class="search" id="input-search" placeholder="Search">
    </div>
    <ul class="search-results"></ul>
    <div id="toc">
    </div>
  </div>
  <div class="page-wrapper">
    <div class="dark-box"></div>
    <div class="content">
      <h1 id="introduction">Introduction</h1>

      <p>All requests must have the following headers set:</p>

      <table><thead>
        <tr>
          <th>Header</th>
          <th>Value</th>
        </tr>
        </thead><tbody>
        <tr>
          <td>Content-Type</td>
          <td>application/json</td>
        </tr>
        </tbody></table>

      <aside class="notice">Where a request body is set, the body must be in valid json format.</aside>

      <h1 id="authentication">Authentication</h1>

      <p>The Reflow API uses <a href="https://en.wikipedia.org/wiki/Basic_access_authentication">HTTP Basic Auth</a> for authenticating requests. A username and password will be provided to you. You will need to set the Authorization header in the following format.</p>

      <p><code class="prettyprint">Authorization: Basic dXNlcm5hbWU6cGFzc3dvcmQxMjM=</code></p>

      <aside class="notice">You will need a username and password and have the authorization header set to be able to communicate with the api.</aside>

      <h1 id="responses">Responses</h1>

      <h2 id="success-response">Success response</h2>

      <blockquote>
        <h3 id="examples">Examples</h3>
      </blockquote>
<pre class="highlight json tab-json"><code><span class="p">{</span><span class="w">
  </span><span class="s2">"status"</span><span class="p">:</span><span class="w"> </span><span class="s2">"success"</span><span class="p">,</span><span class="w">
  </span><span class="s2">"data"</span><span class="w"> </span><span class="p">:</span><span class="w"> </span><span class="kc">null</span><span class="w">
</span><span class="p">}</span><span class="w">


</span><span class="p">{</span><span class="w">
  </span><span class="s2">"status"</span><span class="p">:</span><span class="w"> </span><span class="s2">"success"</span><span class="p">,</span><span class="w">
  </span><span class="s2">"data"</span><span class="w"> </span><span class="p">:</span><span class="w"> </span><span class="p">{</span><span class="w">
    </span><span class="s2">"item"</span><span class="w"> </span><span class="p">:</span><span class="w"> </span><span class="p">{}</span><span class="w">
  </span><span class="p">}</span><span class="w">
</span><span class="p">}</span><span class="w">


</span><span class="p">{</span><span class="w">
  </span><span class="s2">"status"</span><span class="p">:</span><span class="w"> </span><span class="s2">"success"</span><span class="p">,</span><span class="w">
  </span><span class="s2">"data"</span><span class="w"> </span><span class="p">:</span><span class="w"> </span><span class="p">{</span><span class="w">
    </span><span class="s2">"items"</span><span class="w"> </span><span class="p">:</span><span class="w"> </span><span class="p">[]</span><span class="w">
  </span><span class="p">}</span><span class="w">
</span><span class="p">}</span><span class="w">
</span></code></pre>
      <p>All successful responses will return a json object with a <code class="prettyprint">status</code> key of &ldquo;success&rdquo; and a <code class="prettyprint">data</code> key. This <code class="prettyprint">data</code> key will always be an object or null.</p>

      <p>Requests that specify the resource such as PUT/POST (or GET when a resource &lt;ID&gt; is specified) will return the resource object inside the <code class="prettyprint">data</code> object with a key of <code class="prettyprint">item</code>.</p>

      <p>Requests that return multiple resources such as a GET request, will return an array of resource objects inside the <code class="prettyprint">data</code> object with a key of <code class="prettyprint">items</code>.</p>

      <h2 id="error-response">Error response</h2>

      <blockquote>
        <h3 id="example">Example</h3>
      </blockquote>
<pre class="highlight json tab-json"><code><span class="p">{</span><span class="w">
  </span><span class="s2">"status"</span><span class="p">:</span><span class="w"> </span><span class="s2">"error"</span><span class="p">,</span><span class="w">
  </span><span class="s2">"code"</span><span class="p">:</span><span class="w"> </span><span class="mi">404</span><span class="p">,</span><span class="w">
  </span><span class="s2">"message"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Item not found"</span><span class="w">
</span><span class="p">}</span><span class="w">
</span></code></pre>
      <p>All errors return a json object with a <code class="prettyprint">status</code> key of &ldquo;error&rdquo;. The error <code class="prettyprint">code</code> and error <code class="prettyprint">message</code> keys are also always set in this object. If debugging is turned on in the api, additional data will appear in this object.</p>

      <h3 id="error-codes">Error codes</h3>

      <p>We use the following error codes:</p>

      <table><thead>
        <tr>
          <th>Error Code</th>
          <th>Meaning</th>
        </tr>
        </thead><tbody>
        <tr>
          <td>400</td>
          <td>Bad Request</td>
        </tr>
        <tr>
          <td>401</td>
          <td>Unauthorized</td>
        </tr>
        <tr>
          <td>403</td>
          <td>Forbidden</td>
        </tr>
        <tr>
          <td>404</td>
          <td>Not Found</td>
        </tr>
        <tr>
          <td>405</td>
          <td>Method Not Allowed</td>
        </tr>
        <tr>
          <td>500</td>
          <td>Internal Server Error</td>
        </tr>
        <tr>
          <td>501</td>
          <td>Not Implemented</td>
        </tr>
        <tr>
          <td>503</td>
          <td>Service Unavailable</td>
        </tr>
        </tbody></table>

      <h3 id="suppressing-error-codes">Suppressing Error Codes</h3>

      <p>Adding <code class="prettyprint">suppress_response_codes</code> to the header or GET string will ensure a HTTP 200 response is always returned. This is for legacy systems where codes other than 200 are not passed back to the handling code.</p>

      <h1 id="users">Users</h1>

      <h2 id="get-all-users">Get All Users</h2>

      <blockquote>
        <h3 id="example-request">Example Request</h3>

        <p>/v1/api/users?limit=10&amp;offset=0</p>

        <h3 id="example-response">Example Response</h3>
      </blockquote>
<pre class="highlight json tab-json"><code><span class="p">{</span><span class="w">
  </span><span class="s2">"status"</span><span class="p">:</span><span class="w"> </span><span class="s2">"success"</span><span class="p">,</span><span class="w">
  </span><span class="s2">"data"</span><span class="p">:</span><span class="w"> </span><span class="p">{</span><span class="w">
    </span><span class="s2">"total"</span><span class="p">:</span><span class="w"> </span><span class="mi">1</span><span class="p">,</span><span class="w">
    </span><span class="s2">"limit"</span><span class="p">:</span><span class="w"> </span><span class="mi">10</span><span class="p">,</span><span class="w">
    </span><span class="s2">"offset"</span><span class="p">:</span><span class="w"> </span><span class="mi">0</span><span class="p">,</span><span class="w">
    </span><span class="s2">"count"</span><span class="p">:</span><span class="w"> </span><span class="mi">1</span><span class="p">,</span><span class="w">
    </span><span class="s2">"items"</span><span class="p">:</span><span class="w"> </span><span class="p">[</span><span class="w">
      </span><span class="p">{</span><span class="w">
        </span><span class="s2">"id"</span><span class="p">:</span><span class="w"> </span><span class="mi">1</span><span class="p">,</span><span class="w">
        </span><span class="s2">"name"</span><span class="p">:</span><span class="w"> </span><span class="s2">"admin"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"email"</span><span class="p">:</span><span class="w"> </span><span class="s2">"admin_demo@reactor15.com"</span><span class="w">
      </span><span class="p">}</span><span class="w">
    </span><span class="p">]</span><span class="w">
  </span><span class="p">}</span><span class="w">
</span><span class="p">}</span><span class="w">
</span></code></pre>
      <p>This endpoint retrieves all users.</p>

      <h3 id="http-request">HTTP Request</h3>

      <p><code class="prettyprint">GET /v1/api/users</code></p>

      <h3 id="header-query-parameters">Header / Query Parameters</h3>

      <p>The following parameters can be set as headers or be written as part of the GET string</p>

      <table><thead>
        <tr>
          <th>Parameter</th>
          <th>Default</th>
          <th>Description</th>
        </tr>
        </thead><tbody>
        <tr>
          <td>limit</td>
          <td>10</td>
          <td>The maximum number of items to return</td>
        </tr>
        <tr>
          <td>offset</td>
          <td>0</td>
          <td>The number of items to skip</td>
        </tr>
        </tbody></table>

      <h1 id="jobs">Jobs</h1>

      <h2 id="get-all-jobs">Get All Jobs</h2>

      <blockquote>
        <h3 id="example-request">Example Request</h3>

        <p>/v1/api/jobs?limit=1&amp;offset=0&amp;dateUpdated=gte:2017-01-01%2015:00:00</p>

        <h3 id="example-response">Example Response</h3>
      </blockquote>
<pre class="highlight json tab-json"><code><span class="p">{</span><span class="w">
  </span><span class="s2">"status"</span><span class="p">:</span><span class="w"> </span><span class="s2">"success"</span><span class="p">,</span><span class="w">
  </span><span class="s2">"data"</span><span class="p">:</span><span class="w"> </span><span class="p">{</span><span class="w">
    </span><span class="s2">"total"</span><span class="p">:</span><span class="w"> </span><span class="mi">1</span><span class="p">,</span><span class="w">
    </span><span class="s2">"limit"</span><span class="p">:</span><span class="w"> </span><span class="mi">10</span><span class="p">,</span><span class="w">
    </span><span class="s2">"offset"</span><span class="p">:</span><span class="w"> </span><span class="mi">0</span><span class="p">,</span><span class="w">
    </span><span class="s2">"count"</span><span class="p">:</span><span class="w"> </span><span class="mi">1</span><span class="p">,</span><span class="w">
    </span><span class="s2">"items"</span><span class="p">:</span><span class="w"> </span><span class="p">[</span><span class="w">
      </span><span class="p">{</span><span class="w">
        </span><span class="s2">"id"</span><span class="p">:</span><span class="w"> </span><span class="mi">2000</span><span class="p">,</span><span class="w">
        </span><span class="s2">"title"</span><span class="p">:</span><span class="w"> </span><span class="s2">"00000009"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"post_at"</span><span class="p">:</span><span class="w"> </span><span class="s2">"2017-04-06 08:47:14"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"issuedDate"</span><span class="p">:</span><span class="w"> </span><span class="s2">"2017-04-05 08:24:00"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"targetCompletionDate"</span><span class="p">:</span><span class="w"> </span><span class="s2">"2017-04-06 08:24:00"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"priority"</span><span class="p">:</span><span class="w"> </span><span class="s2">"B"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"networkType"</span><span class="p">:</span><span class="w"> </span><span class="s2">"CW"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"inspectorsName"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Joe Blogs"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"notes"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
        </span><span class="s2">"jobClass"</span><span class="p">:</span><span class="w"> </span><span class="s2">"CAT1"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"jobShortDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Pothole (Med)"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"jobDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"5no 'Pothole (Med)'"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"jobImage"</span><span class="p">:</span><span class="w"> </span><span class="p">[],</span><span class="w">
        </span><span class="s2">"usrn"</span><span class="p">:</span><span class="w"> </span><span class="mi">30201239</span><span class="p">,</span><span class="w">
        </span><span class="s2">"street"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Demo WAY"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"town"</span><span class="p">:</span><span class="w"> </span><span class="s2">"PLYMOUTH"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"houseNo"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
        </span><span class="s2">"houseName"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
        </span><span class="s2">"addressDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Pothole (Med)"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"locationDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"12 demo Way, Plymouth, United Kingdom, PL3 4SW"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"defectType"</span><span class="p">:</span><span class="w"> </span><span class="s2">"BLPOM"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"defectStatus"</span><span class="p">:</span><span class="w"> </span><span class="s2">"4"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"defectLocationDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"COUNCILLOR REPORT.Several holes in close proximity on YELLOWBRICK Way requiring attention."</span><span class="p">,</span><span class="w">
        </span><span class="s2">"easting"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
        </span><span class="s2">"northing"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
        </span><span class="s2">"defectNotes"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
        </span><span class="s2">"defectDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Pothole (Med)"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"defectLength"</span><span class="p">:</span><span class="w"> </span><span class="mi">0</span><span class="p">,</span><span class="w">
        </span><span class="s2">"defectWidth"</span><span class="p">:</span><span class="w"> </span><span class="mi">0</span><span class="p">,</span><span class="w">
        </span><span class="s2">"defectDepth"</span><span class="p">:</span><span class="w"> </span><span class="mi">0</span><span class="p">,</span><span class="w">
        </span><span class="s2">"origin"</span><span class="p">:</span><span class="w"> </span><span class="s2">"3RD PARTY"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"boq"</span><span class="p">:</span><span class="w"> </span><span class="p">[</span><span class="w">
          </span><span class="p">{</span><span class="w">
            </span><span class="s2">"sorCode"</span><span class="p">:</span><span class="w"> </span><span class="s2">"73.020.030"</span><span class="p">,</span><span class="w">
            </span><span class="s2">"sorDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"AC 10 dense surf in an individual repair area exceeding 1 but not exceeding 2m?"</span><span class="p">,</span><span class="w">
            </span><span class="s2">"itemLineID"</span><span class="p">:</span><span class="w"> </span><span class="s2">"000025"</span><span class="p">,</span><span class="w">
            </span><span class="s2">"quantity"</span><span class="p">:</span><span class="w"> </span><span class="mi">5</span><span class="p">,</span><span class="w">
            </span><span class="s2">"actualQuantity"</span><span class="p">:</span><span class="w"> </span><span class="s2">"7"</span><span class="p">,</span><span class="w">
            </span><span class="s2">"unit"</span><span class="p">:</span><span class="w"> </span><span class="s2">"no"</span><span class="p">,</span><span class="w">
            </span><span class="s2">"rateBand"</span><span class="p">:</span><span class="w"> </span><span class="mi">1</span><span class="w">
          </span><span class="p">},</span><span class="w">
          </span><span class="p">{</span><span class="w">
            </span><span class="s2">"sorCode"</span><span class="p">:</span><span class="w"> </span><span class="s2">"73.020.045"</span><span class="p">,</span><span class="w">
            </span><span class="s2">"sorDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Additional 5mm increments"</span><span class="p">,</span><span class="w">
            </span><span class="s2">"itemLineID"</span><span class="p">:</span><span class="w"> </span><span class="s2">"000026"</span><span class="p">,</span><span class="w">
            </span><span class="s2">"quantity"</span><span class="p">:</span><span class="w"> </span><span class="mi">0</span><span class="p">,</span><span class="w">
            </span><span class="s2">"actualQuantity"</span><span class="p">:</span><span class="w"> </span><span class="s2">"1"</span><span class="p">,</span><span class="w">
            </span><span class="s2">"unit"</span><span class="p">:</span><span class="w"> </span><span class="s2">"m2"</span><span class="p">,</span><span class="w">
            </span><span class="s2">"rateBand"</span><span class="p">:</span><span class="w"> </span><span class="mi">0</span><span class="w">
          </span><span class="p">}</span><span class="w">
        </span><span class="p">],</span><span class="w">
        </span><span class="s2">"additionalItems"</span><span class="p">:</span><span class="w"> </span><span class="p">[</span><span class="w">
          </span><span class="p">{</span><span class="w">
            </span><span class="s2">"sorCode"</span><span class="p">:</span><span class="w"> </span><span class="s2">"73.035.020"</span><span class="p">,</span><span class="w">
            </span><span class="s2">"sorDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Extra under for response time 28 days"</span><span class="p">,</span><span class="w">
            </span><span class="s2">"quantity"</span><span class="p">:</span><span class="w"> </span><span class="mi">5</span><span class="p">,</span><span class="w">
            </span><span class="s2">"unit"</span><span class="p">:</span><span class="w"> </span><span class="s2">"%"</span><span class="w">
          </span><span class="p">},</span><span class="w">
          </span><span class="p">{</span><span class="w">
            </span><span class="s2">"sorCode"</span><span class="p">:</span><span class="w"> </span><span class="s2">"73.060.020"</span><span class="p">,</span><span class="w">
            </span><span class="s2">"sorDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Yellow line marking, surface area covered/per 5M2"</span><span class="p">,</span><span class="w">
            </span><span class="s2">"quantity"</span><span class="p">:</span><span class="w"> </span><span class="mi">6</span><span class="p">,</span><span class="w">
            </span><span class="s2">"unit"</span><span class="p">:</span><span class="w"> </span><span class="s2">"no"</span><span class="w">
          </span><span class="p">},</span><span class="w">
          </span><span class="p">{</span><span class="w">
            </span><span class="s2">"sorCode"</span><span class="p">:</span><span class="w"> </span><span class="s2">"73.035.020"</span><span class="p">,</span><span class="w">
            </span><span class="s2">"sorDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Extra under for response time 28 days"</span><span class="p">,</span><span class="w">
            </span><span class="s2">"quantity"</span><span class="p">:</span><span class="w"> </span><span class="mi">5</span><span class="p">,</span><span class="w">
            </span><span class="s2">"unit"</span><span class="p">:</span><span class="w"> </span><span class="s2">"%"</span><span class="w">
          </span><span class="p">},</span><span class="w">
          </span><span class="p">{</span><span class="w">
            </span><span class="s2">"sorCode"</span><span class="p">:</span><span class="w"> </span><span class="s2">"73.060.020"</span><span class="p">,</span><span class="w">
            </span><span class="s2">"sorDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Yellow line marking, surface area covered/per 5M2"</span><span class="p">,</span><span class="w">
            </span><span class="s2">"quantity"</span><span class="p">:</span><span class="w"> </span><span class="mi">6</span><span class="p">,</span><span class="w">
            </span><span class="s2">"unit"</span><span class="p">:</span><span class="w"> </span><span class="s2">"no"</span><span class="w">
          </span><span class="p">}</span><span class="w">
        </span><span class="p">],</span><span class="w">
        </span><span class="s2">"projectStatus"</span><span class="p">:</span><span class="w"> </span><span class="s2">"0"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"depart"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
        </span><span class="s2">"arrive"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
        </span><span class="s2">"startWork"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
        </span><span class="s2">"finishWork"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
        </span><span class="s2">"outcome"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
        </span><span class="s2">"outcomeComments"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
        </span><span class="s2">"onHoldDate"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
        </span><span class="s2">"onHoldReason"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
        </span><span class="s2">"photoBeforeUrl"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
        </span><span class="s2">"photoAfterUrl"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
        </span><span class="s2">"workType"</span><span class="p">:</span><span class="w"> </span><span class="s2">"reactiveEvent"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"assignedTo"</span><span class="p">:</span><span class="w"> </span><span class="p">[</span><span class="w">
          </span><span class="mi">1</span><span class="w">
        </span><span class="p">],</span><span class="w">
        </span><span class="s2">"startDate"</span><span class="p">:</span><span class="w"> </span><span class="s2">"2017-04-05 10:20:00"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"endDate"</span><span class="p">:</span><span class="w"> </span><span class="s2">"2017-04-05 23:59:59"</span><span class="w">
      </span><span class="p">}</span><span class="w">
    </span><span class="p">]</span><span class="w">
  </span><span class="p">}</span><span class="w">
</span><span class="p">}</span><span class="w">
</span></code></pre>
      <p>This endpoint retrieves all jobs.</p>

      <h3 id="http-request">HTTP Request</h3>

      <p><code class="prettyprint">GET /v1/api/jobs</code></p>

      <h3 id="header-query-parameters">Header / Query Parameters</h3>

      <p>The following parameters can be set as headers or be written as part of the GET string</p>

      <table><thead>
        <tr>
          <th>Parameter</th>
          <th>Default</th>
          <th>Description</th>
        </tr>
        </thead><tbody>
        <tr>
          <td>limit</td>
          <td>10</td>
          <td>The maximum number of items to return</td>
        </tr>
        <tr>
          <td>offset</td>
          <td>0</td>
          <td>The number of items to skip</td>
        </tr>
        </tbody></table>

      <h3 id="search-parameter">Search Parameter</h3>

      <p>The q parameter can be set in the GET string to search for a specific term within all the job data</p>

      <table><thead>
        <tr>
          <th>Parameter</th>
          <th>Default</th>
          <th>Description</th>
        </tr>
        </thead><tbody>
        <tr>
          <td>q</td>
          <td>null</td>
          <td>Search term across job fields</td>
        </tr>
        </tbody></table>

      <h3 id="content-searching">Content Searching</h3>

      <p>It is possible to search for specific field values with more advanced logic.</p>

      <p>Prefixing field values with a key will specify the search type. Fields values without a prefix, will default to eq:.</p>

      <table><thead>
        <tr>
          <th>Prefix</th>
          <th>Description</th>
        </tr>
        </thead><tbody>
        <tr>
          <td>eq:</td>
          <td>Equals</td>
        </tr>
        <tr>
          <td>lt:</td>
          <td>Less Than</td>
        </tr>
        <tr>
          <td>lte:</td>
          <td>Less Than or Equal To</td>
        </tr>
        <tr>
          <td>gt:</td>
          <td>Greater Than</td>
        </tr>
        <tr>
          <td>gte:</td>
          <td>Greater Than or Equal To</td>
        </tr>
        </tbody></table>

      <p>It is also possible to combine 2 queries on 1 field using by adding <code class="prettyprint">AND</code> or <code class="prettyprint">OR</code> in-between the two values you wish to use. For example, searching between two dates for a field called &lsquo;dateUpdated&rsquo; would be <code class="prettyprint">dateUpdated=gte:2017-01-01T00:00:00 AND lte:2018-01-01T00:00:00</code>.</p>

      <h3 id="content-search-parameters">Content Search Parameters</h3>

      <p>The following parameters are searchable with the logic above. They are specified in the GET string.</p>

      <table><thead>
        <tr>
          <th>Parameter</th>
          <th>Description</th>
        </tr>
        </thead><tbody>
        <tr>
          <td>title</td>
          <td>The title of the job</td>
        </tr>
        <tr>
          <td>dateUpdated</td>
          <td>When the job was last updated in the system (UTC)</td>
        </tr>
        </tbody></table>

      <h2 id="get-a-specific-job">Get a Specific Job</h2>

      <blockquote>
        <h3 id="example-request">Example Request</h3>

        <p>/v1/api/jobs/1058</p>

        <h3 id="example-response">Example Response</h3>
      </blockquote>
<pre class="highlight json tab-json"><code><span class="p">{</span><span class="w">
  </span><span class="s2">"status"</span><span class="p">:</span><span class="w"> </span><span class="s2">"success"</span><span class="p">,</span><span class="w">
  </span><span class="s2">"data"</span><span class="p">:</span><span class="w"> </span><span class="p">{</span><span class="w">
    </span><span class="s2">"item"</span><span class="p">:</span><span class="w"> </span><span class="p">{</span><span class="w">
      </span><span class="s2">"id"</span><span class="p">:</span><span class="w"> </span><span class="mi">2000</span><span class="p">,</span><span class="w">
      </span><span class="s2">"title"</span><span class="p">:</span><span class="w"> </span><span class="s2">"00000009"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"post_at"</span><span class="p">:</span><span class="w"> </span><span class="s2">"2017-04-06 08:47:14"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"issuedDate"</span><span class="p">:</span><span class="w"> </span><span class="s2">"2017-04-05 08:24:00"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"targetCompletionDate"</span><span class="p">:</span><span class="w"> </span><span class="s2">"2017-04-06 08:24:00"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"priority"</span><span class="p">:</span><span class="w"> </span><span class="s2">"B"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"networkType"</span><span class="p">:</span><span class="w"> </span><span class="s2">"CW"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"inspectorsName"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Joe Blogs"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"notes"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
      </span><span class="s2">"jobClass"</span><span class="p">:</span><span class="w"> </span><span class="s2">"CAT1"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"jobShortDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Pothole (Med)"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"jobDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"5no 'Pothole (Med)'"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"jobImage"</span><span class="p">:</span><span class="w"> </span><span class="p">[],</span><span class="w">
      </span><span class="s2">"usrn"</span><span class="p">:</span><span class="w"> </span><span class="mi">30201239</span><span class="p">,</span><span class="w">
      </span><span class="s2">"street"</span><span class="p">:</span><span class="w"> </span><span class="s2">"YELLOWBRICK WAY"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"town"</span><span class="p">:</span><span class="w"> </span><span class="s2">"PLYMOUTH"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"houseNo"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
      </span><span class="s2">"houseName"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
      </span><span class="s2">"addressDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Pothole (Med)"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"locationDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"99 YELLOWBRICK Way, Plymouth, United Kingdom, PL3 4ZZ"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"defectType"</span><span class="p">:</span><span class="w"> </span><span class="s2">"BLPOM"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"defectStatus"</span><span class="p">:</span><span class="w"> </span><span class="s2">"4"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"defectLocationDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"COUNCILLOR REPORT.Several holes in close proximity on YELLOWBRICK Way requiring attention."</span><span class="p">,</span><span class="w">
      </span><span class="s2">"easting"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
      </span><span class="s2">"northing"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
      </span><span class="s2">"defectNotes"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
      </span><span class="s2">"defectDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Pothole (Med)"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"defectLength"</span><span class="p">:</span><span class="w"> </span><span class="mi">0</span><span class="p">,</span><span class="w">
      </span><span class="s2">"defectWidth"</span><span class="p">:</span><span class="w"> </span><span class="mi">0</span><span class="p">,</span><span class="w">
      </span><span class="s2">"defectDepth"</span><span class="p">:</span><span class="w"> </span><span class="mi">0</span><span class="p">,</span><span class="w">
      </span><span class="s2">"origin"</span><span class="p">:</span><span class="w"> </span><span class="s2">"3RD PARTY"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"boq"</span><span class="p">:</span><span class="w"> </span><span class="p">[</span><span class="w">
        </span><span class="p">{</span><span class="w">
          </span><span class="s2">"sorCode"</span><span class="p">:</span><span class="w"> </span><span class="s2">"73.020.030"</span><span class="p">,</span><span class="w">
          </span><span class="s2">"sorDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"AC 10 dense surf in an individual repair area exceeding 1 but not exceeding 2m?"</span><span class="p">,</span><span class="w">
          </span><span class="s2">"itemLineID"</span><span class="p">:</span><span class="w"> </span><span class="s2">"000025"</span><span class="p">,</span><span class="w">
          </span><span class="s2">"quantity"</span><span class="p">:</span><span class="w"> </span><span class="mi">5</span><span class="p">,</span><span class="w">
          </span><span class="s2">"actualQuantity"</span><span class="p">:</span><span class="w"> </span><span class="s2">"7"</span><span class="p">,</span><span class="w">
          </span><span class="s2">"unit"</span><span class="p">:</span><span class="w"> </span><span class="s2">"no"</span><span class="p">,</span><span class="w">
          </span><span class="s2">"rateBand"</span><span class="p">:</span><span class="w"> </span><span class="mi">1</span><span class="w">
        </span><span class="p">},</span><span class="w">
        </span><span class="p">{</span><span class="w">
          </span><span class="s2">"sorCode"</span><span class="p">:</span><span class="w"> </span><span class="s2">"73.020.045"</span><span class="p">,</span><span class="w">
          </span><span class="s2">"sorDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Additional 5mm increments"</span><span class="p">,</span><span class="w">
          </span><span class="s2">"itemLineID"</span><span class="p">:</span><span class="w"> </span><span class="s2">"000026"</span><span class="p">,</span><span class="w">
          </span><span class="s2">"quantity"</span><span class="p">:</span><span class="w"> </span><span class="mi">0</span><span class="p">,</span><span class="w">
          </span><span class="s2">"actualQuantity"</span><span class="p">:</span><span class="w"> </span><span class="s2">"1"</span><span class="p">,</span><span class="w">
          </span><span class="s2">"unit"</span><span class="p">:</span><span class="w"> </span><span class="s2">"m2"</span><span class="p">,</span><span class="w">
          </span><span class="s2">"rateBand"</span><span class="p">:</span><span class="w"> </span><span class="mi">0</span><span class="w">
        </span><span class="p">}</span><span class="w">
      </span><span class="p">],</span><span class="w">
      </span><span class="s2">"additionalItems"</span><span class="p">:</span><span class="w"> </span><span class="p">[</span><span class="w">
        </span><span class="p">{</span><span class="w">
          </span><span class="s2">"sorCode"</span><span class="p">:</span><span class="w"> </span><span class="s2">"73.035.020"</span><span class="p">,</span><span class="w">
          </span><span class="s2">"sorDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Extra under for response time 28 days"</span><span class="p">,</span><span class="w">
          </span><span class="s2">"quantity"</span><span class="p">:</span><span class="w"> </span><span class="mi">5</span><span class="p">,</span><span class="w">
          </span><span class="s2">"unit"</span><span class="p">:</span><span class="w"> </span><span class="s2">"%"</span><span class="w">
        </span><span class="p">},</span><span class="w">
        </span><span class="p">{</span><span class="w">
          </span><span class="s2">"sorCode"</span><span class="p">:</span><span class="w"> </span><span class="s2">"73.060.020"</span><span class="p">,</span><span class="w">
          </span><span class="s2">"sorDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Yellow line marking, surface area covered/per 5M2"</span><span class="p">,</span><span class="w">
          </span><span class="s2">"quantity"</span><span class="p">:</span><span class="w"> </span><span class="mi">6</span><span class="p">,</span><span class="w">
          </span><span class="s2">"unit"</span><span class="p">:</span><span class="w"> </span><span class="s2">"no"</span><span class="w">
        </span><span class="p">},</span><span class="w">
        </span><span class="p">{</span><span class="w">
          </span><span class="s2">"sorCode"</span><span class="p">:</span><span class="w"> </span><span class="s2">"73.035.020"</span><span class="p">,</span><span class="w">
          </span><span class="s2">"sorDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Extra under for response time 28 days"</span><span class="p">,</span><span class="w">
          </span><span class="s2">"quantity"</span><span class="p">:</span><span class="w"> </span><span class="mi">5</span><span class="p">,</span><span class="w">
          </span><span class="s2">"unit"</span><span class="p">:</span><span class="w"> </span><span class="s2">"%"</span><span class="w">
        </span><span class="p">},</span><span class="w">
        </span><span class="p">{</span><span class="w">
          </span><span class="s2">"sorCode"</span><span class="p">:</span><span class="w"> </span><span class="s2">"73.060.020"</span><span class="p">,</span><span class="w">
          </span><span class="s2">"sorDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Yellow line marking, surface area covered/per 5M2"</span><span class="p">,</span><span class="w">
          </span><span class="s2">"quantity"</span><span class="p">:</span><span class="w"> </span><span class="mi">6</span><span class="p">,</span><span class="w">
          </span><span class="s2">"unit"</span><span class="p">:</span><span class="w"> </span><span class="s2">"no"</span><span class="w">
        </span><span class="p">}</span><span class="w">
      </span><span class="p">],</span><span class="w">
      </span><span class="s2">"projectStatus"</span><span class="p">:</span><span class="w"> </span><span class="s2">"0"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"depart"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
      </span><span class="s2">"arrive"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
      </span><span class="s2">"startWork"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
      </span><span class="s2">"finishWork"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
      </span><span class="s2">"outcome"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
      </span><span class="s2">"outcomeComments"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
      </span><span class="s2">"onHoldDate"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
      </span><span class="s2">"onHoldReason"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
      </span><span class="s2">"photoBeforeUrl"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
      </span><span class="s2">"photoAfterUrl"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
      </span><span class="s2">"workType"</span><span class="p">:</span><span class="w"> </span><span class="s2">"reactiveEvent"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"assignedTo"</span><span class="p">:</span><span class="w"> </span><span class="p">[</span><span class="w">
        </span><span class="mi">1</span><span class="w">
      </span><span class="p">],</span><span class="w">
      </span><span class="s2">"startDate"</span><span class="p">:</span><span class="w"> </span><span class="s2">"2017-04-05 10:20:00"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"endDate"</span><span class="p">:</span><span class="w"> </span><span class="s2">"2017-04-05 23:59:59"</span><span class="w">
    </span><span class="p">}</span><span class="w">
  </span><span class="p">}</span><span class="w">
</span><span class="p">}</span><span class="w">
</span></code></pre>
      <p>This endpoint retrieves a specific job.</p>

      <h3 id="http-request">HTTP Request</h3>

      <p><code class="prettyprint">GET /v1/api/jobs/&lt;ID&gt;</code></p>

      <h3 id="url-parameters">URL Parameters</h3>

      <table><thead>
        <tr>
          <th>Parameter</th>
          <th>Description</th>
        </tr>
        </thead><tbody>
        <tr>
          <td>ID</td>
          <td>The ID of the job to retrieve</td>
        </tr>
        </tbody></table>

      <h2 id="create-a-job">Create a Job</h2>

      <p>This endpoint creates a new job in the system.</p>

      <blockquote>
        <h3 id="example-request">Example Request</h3>

        <p>/v1/api/jobs</p>
      </blockquote>
<pre class="highlight json tab-json"><code><span class="p">{</span><span class="w">
      </span><span class="s2">"title"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Test Job"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"issuedDate"</span><span class="p">:</span><span class="w"> </span><span class="s2">"2017-03-17 00:00:00"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"targetCompletionDate"</span><span class="p">:</span><span class="w"> </span><span class="s2">"2017-03-31 00:00:00"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"priority"</span><span class="p">:</span><span class="w"> </span><span class="s2">"D"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"networkType"</span><span class="p">:</span><span class="w"> </span><span class="s2">"CW"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"inspectorsName"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Highways Inspector 1"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"notes"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Job notes"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"jobShortDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Pothole"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"jobDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Pothole description here"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"usrn"</span><span class="p">:</span><span class="w"> </span><span class="mi">30202030</span><span class="p">,</span><span class="w">
      </span><span class="s2">"street"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Macadam Road"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"town"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Plymouth"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"houseNo"</span><span class="p">:</span><span class="w"> </span><span class="s2">"5a"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"houseName"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Wade Towers"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"locationDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Defect location Plymouth"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"defectType"</span><span class="p">:</span><span class="w"> </span><span class="s2">"BLPOM"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"jobClass"</span><span class="p">:</span><span class="w"> </span><span class="s2">"CAT2"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"defectStatus"</span><span class="p">:</span><span class="w"> </span><span class="s2">"4"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"addressDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Pothole Address"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"defectLocationDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Defect location description"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"defectNotes"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Defect notes"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"origin"</span><span class="p">:</span><span class="w"> </span><span class="s2">"3rd Party"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"defectDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Pothole (Med)"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"jobImage"</span><span class="p">:</span><span class="w"> </span><span class="p">[</span><span class="w">
        </span><span class="p">{</span><span class="w">
          </span><span class="s2">"title"</span><span class="p">:</span><span class="w"> </span><span class="s2">"4117B01B 935F 4Bf2 9527 5F6421969F54"</span><span class="p">,</span><span class="w">
          </span><span class="s2">"base64"</span><span class="w"> </span><span class="p">:</span><span class="w"> </span><span class="s2">"base64encoded file here..."</span><span class="w">
        </span><span class="p">}</span><span class="w">
      </span><span class="p">],</span><span class="w">
      </span><span class="s2">"defectLength"</span><span class="p">:</span><span class="w"> </span><span class="mf">0.5</span><span class="p">,</span><span class="w">
      </span><span class="s2">"defectWidth"</span><span class="p">:</span><span class="w"> </span><span class="mf">0.5</span><span class="p">,</span><span class="w">
      </span><span class="s2">"defectDepth"</span><span class="p">:</span><span class="w"> </span><span class="mf">0.04</span><span class="p">,</span><span class="w">
      </span><span class="s2">"boq"</span><span class="p">:</span><span class="w"> </span><span class="p">[</span><span class="w">
        </span><span class="p">{</span><span class="w">
          </span><span class="s2">"quantity"</span><span class="p">:</span><span class="w"> </span><span class="mi">1</span><span class="p">,</span><span class="w">
          </span><span class="s2">"sorCode"</span><span class="p">:</span><span class="w"> </span><span class="s2">"01.025.005"</span><span class="p">,</span><span class="w">
          </span><span class="s2">"sorDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Erection and establishment"</span><span class="p">,</span><span class="w">
          </span><span class="s2">"unit"</span><span class="p">:</span><span class="w"> </span><span class="s2">"no"</span><span class="p">,</span><span class="w">
          </span><span class="s2">"rateBand"</span><span class="p">:</span><span class="w"> </span><span class="mi">1</span><span class="w">
        </span><span class="p">},</span><span class="w">
        </span><span class="p">{</span><span class="w">
          </span><span class="s2">"quantity"</span><span class="p">:</span><span class="w"> </span><span class="mi">1</span><span class="p">,</span><span class="w">
          </span><span class="s2">"sorCode"</span><span class="p">:</span><span class="w"> </span><span class="s2">"07.440.005"</span><span class="p">,</span><span class="w">
          </span><span class="s2">"sorDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Not exceeding 5m"</span><span class="p">,</span><span class="w">
          </span><span class="s2">"unit"</span><span class="p">:</span><span class="w"> </span><span class="s2">"m2"</span><span class="p">,</span><span class="w">
          </span><span class="s2">"rateBand"</span><span class="p">:</span><span class="w"> </span><span class="mi">1</span><span class="w">
        </span><span class="p">}</span><span class="w">
      </span><span class="p">]</span><span class="w">
    </span><span class="p">}</span><span class="w">
  </span><span class="p">}</span><span class="w">
</span><span class="p">}</span><span class="w">

</span></code></pre>
      <blockquote>
        <h3 id="example-response">Example Response</h3>
      </blockquote>
<pre class="highlight json tab-json"><code><span class="p">{</span><span class="w">
  </span><span class="s2">"status"</span><span class="p">:</span><span class="w"> </span><span class="s2">"success"</span><span class="p">,</span><span class="w">
  </span><span class="s2">"data"</span><span class="p">:</span><span class="w"> </span><span class="p">{</span><span class="w">
    </span><span class="s2">"item"</span><span class="p">:</span><span class="w"> </span><span class="p">{</span><span class="w">
      </span><span class="s2">"id"</span><span class="p">:</span><span class="w"> </span><span class="mi">1058</span><span class="p">,</span><span class="w">
      </span><span class="s2">"title"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Test Job"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"post_at"</span><span class="p">:</span><span class="w"> </span><span class="s2">"2017-03-17 19:09:00"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"issuedDate"</span><span class="p">:</span><span class="w"> </span><span class="s2">"2017-03-17 00:00:00"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"targetCompletionDate"</span><span class="p">:</span><span class="w"> </span><span class="s2">"2017-03-31 00:00:00"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"priority"</span><span class="p">:</span><span class="w"> </span><span class="s2">"D"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"networkType"</span><span class="p">:</span><span class="w"> </span><span class="s2">"CW"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"inspectorsName"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Highways Inspector 1"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"notes"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Job notes"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"jobShortDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Pothole"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"jobDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Pothole description here"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"usrn"</span><span class="p">:</span><span class="w"> </span><span class="mi">30202030</span><span class="p">,</span><span class="w">
      </span><span class="s2">"street"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Macadam Road"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"town"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Plymouth"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"houseNo"</span><span class="p">:</span><span class="w"> </span><span class="s2">"5a"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"houseName"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Wade Towers"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"locationDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Defect location Plymouth"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"defectType"</span><span class="p">:</span><span class="w"> </span><span class="s2">"BLPOM"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"jobClass"</span><span class="p">:</span><span class="w"> </span><span class="s2">"CAT2"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"defectStatus"</span><span class="p">:</span><span class="w"> </span><span class="s2">"4"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"addressDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Pothole Address"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"defectLocationDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Defect location description"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"defectNotes"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Defect notes"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"origin"</span><span class="p">:</span><span class="w"> </span><span class="s2">"3rd Party"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"defectDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Pothole (Med)"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"jobImage"</span><span class="p">:</span><span class="w"> </span><span class="p">[</span><span class="w">
        </span><span class="p">{</span><span class="w">
          </span><span class="s2">"id"</span><span class="p">:</span><span class="w"> </span><span class="mi">1057</span><span class="p">,</span><span class="w">
          </span><span class="s2">"title"</span><span class="p">:</span><span class="w"> </span><span class="s2">"4117B01B 935F 4Bf2 9527 5F6421969F54"</span><span class="p">,</span><span class="w">
          </span><span class="s2">"uri"</span><span class="p">:</span><span class="w"> </span><span class="s2">"https://demo.re-flow.co.uk/cpresources/404?x=jATwtKSah"</span><span class="p">,</span><span class="w">
          </span><span class="s2">"cache"</span><span class="p">:</span><span class="w"> </span><span class="kc">true</span><span class="p">,</span><span class="w">
          </span><span class="s2">"width"</span><span class="p">:</span><span class="w"> </span><span class="mi">450</span><span class="p">,</span><span class="w">
          </span><span class="s2">"height"</span><span class="p">:</span><span class="w"> </span><span class="mi">800</span><span class="w">
        </span><span class="p">}</span><span class="w">
      </span><span class="p">],</span><span class="w">
      </span><span class="s2">"defectLength"</span><span class="p">:</span><span class="w"> </span><span class="mf">0.5</span><span class="p">,</span><span class="w">
      </span><span class="s2">"defectWidth"</span><span class="p">:</span><span class="w"> </span><span class="mf">0.5</span><span class="p">,</span><span class="w">
      </span><span class="s2">"defectDepth"</span><span class="p">:</span><span class="w"> </span><span class="mf">0.04</span><span class="p">,</span><span class="w">
      </span><span class="s2">"boq"</span><span class="p">:</span><span class="w"> </span><span class="p">[</span><span class="w">
        </span><span class="p">{</span><span class="w">
          </span><span class="s2">"quantity"</span><span class="p">:</span><span class="w"> </span><span class="mi">1</span><span class="p">,</span><span class="w">
          </span><span class="s2">"sorCode"</span><span class="p">:</span><span class="w"> </span><span class="s2">"01.025.005"</span><span class="p">,</span><span class="w">
          </span><span class="s2">"sorDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Erection and establishment"</span><span class="p">,</span><span class="w">
          </span><span class="s2">"unit"</span><span class="p">:</span><span class="w"> </span><span class="s2">"no"</span><span class="p">,</span><span class="w">
          </span><span class="s2">"rateBand"</span><span class="p">:</span><span class="w"> </span><span class="mi">1</span><span class="w">
        </span><span class="p">},</span><span class="w">
        </span><span class="p">{</span><span class="w">
          </span><span class="s2">"quantity"</span><span class="p">:</span><span class="w"> </span><span class="mi">1</span><span class="p">,</span><span class="w">
          </span><span class="s2">"sorCode"</span><span class="p">:</span><span class="w"> </span><span class="s2">"07.440.005"</span><span class="p">,</span><span class="w">
          </span><span class="s2">"sorDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Not exceeding 5m"</span><span class="p">,</span><span class="w">
          </span><span class="s2">"unit"</span><span class="p">:</span><span class="w"> </span><span class="s2">"m2"</span><span class="p">,</span><span class="w">
          </span><span class="s2">"rateBand"</span><span class="p">:</span><span class="w"> </span><span class="mi">1</span><span class="w">
        </span><span class="p">}</span><span class="w">
      </span><span class="p">],</span><span class="w">
      </span><span class="s2">"projectStatus"</span><span class="p">:</span><span class="w"> </span><span class="s2">"3"</span><span class="p">,</span><span class="w">
      </span><span class="s2">"depart"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
      </span><span class="s2">"arrive"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
      </span><span class="s2">"startWork"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
      </span><span class="s2">"outcome"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
      </span><span class="s2">"outcomeComments"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
      </span><span class="s2">"onHoldReason"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
      </span><span class="s2">"photoBeforeUrl"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
      </span><span class="s2">"photoAfterUrl"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
      </span><span class="s2">"onHoldDate"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="w">
    </span><span class="p">}</span><span class="w">
  </span><span class="p">}</span><span class="w">
</span><span class="p">}</span><span class="w">
</span></code></pre>
      <h3 id="http-request">HTTP Request</h3>

      <p><code class="prettyprint">POST /v1/api/jobs</code></p>

      <h3 id="http-body">HTTP Body</h3>

      <p>The HTTP body should contain a json object representing the job data.</p>

      <aside class="notice"><strong>UUID:</strong> Setting a <a href="https://en.wikipedia.org/wiki/Universally_unique_identifier">UUID</a> string will help to ensure a job is never added twice, for example if there is a connection failure halfway through the request. If a job with the same UUID is already found, the posted data will be merged in. This prevents duplicates from occuring if the POST method is called more than once. <strong>If set, the uuid must be globally unique not just unique to a job.</strong></aside>

      <table><thead>
        <tr>
          <th>Json Parameter</th>
          <th>Description</th>
        </tr>
        </thead><tbody>
        <tr>
          <td><strong>uuid</strong></td>
          <td>(string)(optional) universal unique string</td>
        </tr>
        <tr>
          <td>title</td>
          <td>(string) The unique reference for the job [jobNumber]</td>
        </tr>
        <tr>
          <td>workType</td>
          <td>(string) A reference to which kind of actions need to be completed e.g. &lsquo;reactiveEvent&rsquo;</td>
        </tr>
        <tr>
          <td>startDate</td>
          <td>Date the job is scheduled for the user</td>
        </tr>
        <tr>
          <td>assignedTo</td>
          <td>Array of userIds - which users receive the job in the app</td>
        </tr>
        <tr>
          <td>issuedDate</td>
          <td>Date the job was issued to ACME</td>
        </tr>
        <tr>
          <td>targetCompletionDate</td>
          <td>Date ACME are contracted to complete the job by</td>
        </tr>
        <tr>
          <td>priority</td>
          <td>Lookup - A/B/C/D/E/PL</td>
        </tr>
        <tr>
          <td>networkType</td>
          <td>Lookup - CW/AC/KE etc&hellip;</td>
        </tr>
        <tr>
          <td>inspectorsName</td>
          <td>(string)</td>
        </tr>
        <tr>
          <td>notes</td>
          <td>(string)</td>
        </tr>
        <tr>
          <td>jobShortDescription</td>
          <td>(string)</td>
        </tr>
        <tr>
          <td>jobDescription</td>
          <td>(string)</td>
        </tr>
        <tr>
          <td>usrn</td>
          <td>(integer)</td>
        </tr>
        <tr>
          <td>street</td>
          <td>(string)</td>
        </tr>
        <tr>
          <td>town</td>
          <td>(string)</td>
        </tr>
        <tr>
          <td>houseNo</td>
          <td>(string)</td>
        </tr>
        <tr>
          <td>houseName</td>
          <td>(string)</td>
        </tr>
        <tr>
          <td>locationDescription</td>
          <td>(string)</td>
        </tr>
        <tr>
          <td>easting</td>
          <td>(integer)</td>
        </tr>
        <tr>
          <td>northing</td>
          <td>(integer)</td>
        </tr>
        <tr>
          <td>defectType</td>
          <td>Lookup - BLPOS/BLPOM/BLPOL etc&hellip;</td>
        </tr>
        <tr>
          <td>jobClass</td>
          <td>Lookup - EMER/CAT1 etc&hellip;</td>
        </tr>
        <tr>
          <td>defectStatus</td>
          <td>Lookup - 1/2/3 etc..</td>
        </tr>
        <tr>
          <td>addressDescription</td>
          <td>(string)</td>
        </tr>
        <tr>
          <td>defectLocationDescription</td>
          <td>(string)</td>
        </tr>
        <tr>
          <td>defectNotes</td>
          <td>(string)</td>
        </tr>
        <tr>
          <td>origin</td>
          <td>(string)</td>
        </tr>
        <tr>
          <td>defectDescription</td>
          <td>(string)</td>
        </tr>
        <tr>
          <td>defectLength</td>
          <td>(float) 2 decimal place</td>
        </tr>
        <tr>
          <td>defectWidth</td>
          <td>(float) 2 decimal place</td>
        </tr>
        <tr>
          <td>defectDepth</td>
          <td>(float) 3 decimal places</td>
        </tr>
        <tr>
          <td>jobImage</td>
          <td>array of objects</td>
        </tr>
        <tr>
          <td>boq</td>
          <td>array of objects</td>
        </tr>
        </tbody></table>

      <!-- events | array of objects -->

      <h3 id="sending-updating-job-images">Sending / Updating Job Images</h3>

      <p>You can send an array of objects in the <code class="prettyprint">jobImage</code> key. Each object needs to contain a <code class="prettyprint">title</code> and a <code class="prettyprint">base64</code> key. The <code class="prettyprint">title</code> is the display name you want for the file, it does not have to contain a file extension. The <code class="prettyprint">base64</code> key is the base64 encoding of the file you wish to upload.</p>

      <p>You can repeatedly send the data over in this format without having to worry about storing the urls or ids returned. The API will detect if a duplicate file is being sent.</p>

      <!-- Todo: Need to check sending ids / titles works correctly -->

      <!--
      Shouyld we expose events?

      Events Data | Description
      ----------- | -----------
      title | (string) Reference for the job required

      -->

      <h2 id="update-a-job">Update a Job</h2>

      <p>This endpoint updates a job in the system.</p>

      <blockquote>
        <h3 id="example-request">Example Request</h3>

        <p>/v1/api/jobs/1058</p>
      </blockquote>

      <h3 id="http-request">HTTP Request</h3>

      <p><code class="prettyprint">PUT /v1/api/jobs/&lt;ID&gt;</code></p>

      <h3 id="url-parameters">URL Parameters</h3>

      <table><thead>
        <tr>
          <th>Parameter</th>
          <th>Description</th>
        </tr>
        </thead><tbody>
        <tr>
          <td>ID</td>
          <td>The ID of the job to update</td>
        </tr>
        </tbody></table>

      <h3 id="http-body">HTTP Body</h3>

      <p>The same as a POST request</p>

      <aside class="notice"><strong>UUID:</strong> If a uuid is set on a PUT request, the api will check that the id and uuid match. If no uuid was originally set on the job, the job will be assigned the uuid provided.</strong></aside>

      <h2 id="delete-a-job">Delete a Job</h2>

      <p>This endpoint deletes a job from the system.</p>

      <blockquote>
        <h3 id="example-request">Example Request</h3>

        <p>/v1/api/jobs/1058</p>

        <h3 id="example-response">Example Response</h3>
      </blockquote>
<pre class="highlight json tab-json"><code><span class="p">{</span><span class="w">
  </span><span class="s2">"status"</span><span class="p">:</span><span class="w"> </span><span class="s2">"success"</span><span class="p">,</span><span class="w">
  </span><span class="s2">"data"</span><span class="p">:</span><span class="w"> </span><span class="kc">null</span><span class="w">
</span><span class="p">}</span><span class="w">
</span></code></pre>
      <h3 id="http-request">HTTP Request</h3>

      <p><code class="prettyprint">DELETE /v1/api/jobs/&lt;ID&gt;</code></p>

      <h3 id="url-parameters">URL Parameters</h3>

      <table><thead>
        <tr>
          <th>Parameter</th>
          <th>Description</th>
        </tr>
        </thead><tbody>
        <tr>
          <td>ID</td>
          <td>The ID of the job to delete</td>
        </tr>
        </tbody></table>

      <h1 id="form-data">Form Data</h1>

      <h2 id="getting-form-data">Getting form data</h2>

      <p>All form data requests follow this format. The data object returned varies depending on the form name specified.</p>

      <blockquote>
        <h3 id="example-request">Example Request</h3>

        <p>/v1/api/sor?projectId=1058</p>

        <h3 id="example-response">Example Response</h3>
      </blockquote>
<pre class="highlight json tab-json"><code><span class="p">{</span><span class="w">
  </span><span class="s2">"status"</span><span class="p">:</span><span class="w"> </span><span class="s2">"success"</span><span class="p">,</span><span class="w">
  </span><span class="s2">"data"</span><span class="p">:</span><span class="w"> </span><span class="p">{</span><span class="w">
    </span><span class="s2">"total"</span><span class="p">:</span><span class="w"> </span><span class="mi">1</span><span class="p">,</span><span class="w">
    </span><span class="s2">"limit"</span><span class="p">:</span><span class="w"> </span><span class="mi">10</span><span class="p">,</span><span class="w">
    </span><span class="s2">"offset"</span><span class="p">:</span><span class="w"> </span><span class="mi">0</span><span class="p">,</span><span class="w">
    </span><span class="s2">"count"</span><span class="p">:</span><span class="w"> </span><span class="mi">1</span><span class="p">,</span><span class="w">
    </span><span class="s2">"items"</span><span class="p">:</span><span class="w"> </span><span class="p">[</span><span class="w">
      </span><span class="p">{</span><span class="w">
        </span><span class="s2">"id"</span><span class="p">:</span><span class="w"> </span><span class="mi">3</span><span class="p">,</span><span class="w">
        </span><span class="s2">"formStatus"</span><span class="p">:</span><span class="w"> </span><span class="mi">0</span><span class="p">,</span><span class="w">
        </span><span class="s2">"transactionId"</span><span class="p">:</span><span class="w"> </span><span class="s2">"21-03-2017_16-49-42-YOBMHX"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"entryId"</span><span class="p">:</span><span class="w"> </span><span class="mi">1104</span><span class="p">,</span><span class="w">
        </span><span class="s2">"formName"</span><span class="p">:</span><span class="w"> </span><span class="s2">"21-03-2017 16:49:42 - SOR"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"pdfViewed"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
        </span><span class="s2">"projectId"</span><span class="p">:</span><span class="w"> </span><span class="s2">"1058"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"userId"</span><span class="p">:</span><span class="w"> </span><span class="s2">"1"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"tokenId"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
        </span><span class="s2">"userStatusId"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
        </span><span class="s2">"userViewPdfId"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
        </span><span class="s2">"boq"</span><span class="p">:</span><span class="w"> </span><span class="p">[</span><span class="w">
          </span><span class="p">{</span><span class="w">
            </span><span class="s2">"id"</span><span class="p">:</span><span class="w"> </span><span class="mi">5</span><span class="p">,</span><span class="w">
            </span><span class="s2">"actual_quantity"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
            </span><span class="s2">"quantity"</span><span class="p">:</span><span class="w"> </span><span class="mi">1</span><span class="p">,</span><span class="w">
            </span><span class="s2">"sorCode"</span><span class="p">:</span><span class="w"> </span><span class="s2">"01.025.005"</span><span class="p">,</span><span class="w">
            </span><span class="s2">"sorDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Erection and establishment"</span><span class="p">,</span><span class="w">
            </span><span class="s2">"unit"</span><span class="p">:</span><span class="w"> </span><span class="s2">"no"</span><span class="p">,</span><span class="w">
            </span><span class="s2">"rateBand"</span><span class="p">:</span><span class="w"> </span><span class="mi">1</span><span class="w">
          </span><span class="p">},</span><span class="w">
          </span><span class="p">{</span><span class="w">
            </span><span class="s2">"id"</span><span class="p">:</span><span class="w"> </span><span class="mi">6</span><span class="p">,</span><span class="w">
            </span><span class="s2">"actual_quantity"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
            </span><span class="s2">"quantity"</span><span class="p">:</span><span class="w"> </span><span class="mi">1</span><span class="p">,</span><span class="w">
            </span><span class="s2">"sorCode"</span><span class="p">:</span><span class="w"> </span><span class="s2">"07.440.005"</span><span class="p">,</span><span class="w">
            </span><span class="s2">"sorDescription"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Not exceeding 5m"</span><span class="p">,</span><span class="w">
            </span><span class="s2">"unit"</span><span class="p">:</span><span class="w"> </span><span class="s2">"m2"</span><span class="p">,</span><span class="w">
            </span><span class="s2">"rateBand"</span><span class="p">:</span><span class="w"> </span><span class="mi">1</span><span class="w">
          </span><span class="p">}</span><span class="w">
        </span><span class="p">],</span><span class="w">
        </span><span class="s2">"additional_items"</span><span class="p">:</span><span class="w"> </span><span class="p">[</span><span class="w">
          </span><span class="p">{</span><span class="w">
            </span><span class="s2">"id"</span><span class="p">:</span><span class="w"> </span><span class="mi">3</span><span class="p">,</span><span class="w">
            </span><span class="s2">"quantity"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
            </span><span class="s2">"sorCode"</span><span class="p">:</span><span class="w"> </span><span class="s2">"01.569.010"</span><span class="p">,</span><span class="w">
            </span><span class="s2">"unit"</span><span class="p">:</span><span class="w"> </span><span class="s2">"m/day"</span><span class="p">,</span><span class="w">
            </span><span class="s2">"itemId"</span><span class="p">:</span><span class="w"> </span><span class="s2">"1175"</span><span class="w">
          </span><span class="p">}</span><span class="w">
        </span><span class="p">],</span><span class="w">
        </span><span class="s2">"dateUpdated"</span><span class="p">:</span><span class="w"> </span><span class="s2">"2017-03-21 16:49:44"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"dateCreated"</span><span class="p">:</span><span class="w"> </span><span class="s2">"2017-03-21 16:49:44"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"isDraft"</span><span class="p">:</span><span class="w"> </span><span class="kc">true</span><span class="w">
      </span><span class="p">}</span><span class="w">
    </span><span class="p">]</span><span class="w">
  </span><span class="p">}</span><span class="w">
</span><span class="p">}</span><span class="w">
</span></code></pre>
      <blockquote>
        <h3 id="example-request">Example Request</h3>

        <p>/v1/api/RiskAssessment?dateUpdated=gte:2017-03-28T16:00:00</p>

        <h3 id="example-response">Example Response</h3>
      </blockquote>
<pre class="highlight json tab-json"><code><span class="p">{</span><span class="w">
  </span><span class="s2">"status"</span><span class="p">:</span><span class="w"> </span><span class="s2">"success"</span><span class="p">,</span><span class="w">
  </span><span class="s2">"data"</span><span class="p">:</span><span class="w"> </span><span class="p">{</span><span class="w">
    </span><span class="s2">"total"</span><span class="p">:</span><span class="w"> </span><span class="mi">1</span><span class="p">,</span><span class="w">
    </span><span class="s2">"limit"</span><span class="p">:</span><span class="w"> </span><span class="mi">10</span><span class="p">,</span><span class="w">
    </span><span class="s2">"offset"</span><span class="p">:</span><span class="w"> </span><span class="mi">0</span><span class="p">,</span><span class="w">
    </span><span class="s2">"count"</span><span class="p">:</span><span class="w"> </span><span class="mi">1</span><span class="p">,</span><span class="w">
    </span><span class="s2">"items"</span><span class="p">:</span><span class="w"> </span><span class="p">[</span><span class="w">
      </span><span class="p">{</span><span class="w">
        </span><span class="s2">"id"</span><span class="p">:</span><span class="w"> </span><span class="mi">6</span><span class="p">,</span><span class="w">
        </span><span class="s2">"risk_groups"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Broken Cover / lid, Flood / Blocked Drain or gully"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"hazards"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Chemicals/harmful substances, Combustible Materials, Danger from others working nearby, Environment"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"other_hazards"</span><span class="p">:</span><span class="w"> </span><span class="s2">"test hazard"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"risks_covered"</span><span class="p">:</span><span class="w"> </span><span class="s2">"No"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"additional_controls"</span><span class="p">:</span><span class="w"> </span><span class="s2">"test controls"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"control_measures"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Yes"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"briefed"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Yes"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"traffic_count"</span><span class="p">:</span><span class="w"> </span><span class="mi">5221</span><span class="p">,</span><span class="w">
        </span><span class="s2">"tm_setup"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Mobile Works"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"other_tm"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
        </span><span class="s2">"formStatus"</span><span class="p">:</span><span class="w"> </span><span class="mi">1</span><span class="p">,</span><span class="w">
        </span><span class="s2">"transactionId"</span><span class="p">:</span><span class="w"> </span><span class="s2">"28-03-2017_17-33-32-XVRGDL"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"entryId"</span><span class="p">:</span><span class="w"> </span><span class="mi">1392</span><span class="p">,</span><span class="w">
        </span><span class="s2">"formName"</span><span class="p">:</span><span class="w"> </span><span class="s2">"28-03-2017 17:33:34 - Risk Assessment"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"pdfViewed"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
        </span><span class="s2">"signature"</span><span class="p">:</span><span class="w"> </span><span class="p">{</span><span class="w">
          </span><span class="s2">"id"</span><span class="p">:</span><span class="w"> </span><span class="mi">104</span><span class="p">,</span><span class="w">
          </span><span class="s2">"files"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
          </span><span class="s2">"statement"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
          </span><span class="s2">"signatures"</span><span class="p">:</span><span class="w"> </span><span class="p">[</span><span class="w">
            </span><span class="p">{</span><span class="w">
              </span><span class="s2">"id"</span><span class="p">:</span><span class="w"> </span><span class="mi">86</span><span class="p">,</span><span class="w">
              </span><span class="s2">"title"</span><span class="p">:</span><span class="w"> </span><span class="s2">"test"</span><span class="p">,</span><span class="w">
              </span><span class="s2">"url"</span><span class="p">:</span><span class="w"> </span><span class="s2">"https://demo.re-flow.co.uk/cpresources/reflowdata/attachments/forms/signatures/RiskAssessment_sig28-03-2017_17-33-32-WJETMU.jpg"</span><span class="p">,</span><span class="w">
              </span><span class="s2">"date"</span><span class="p">:</span><span class="w"> </span><span class="s2">"2017-03-28 17:33:32"</span><span class="p">,</span><span class="w">
              </span><span class="s2">"path"</span><span class="p">:</span><span class="w"> </span><span class="s2">"/forms/signatures/RiskAssessment_sig28-03-2017_17-33-32-WJETMU.jpg"</span><span class="p">,</span><span class="w">
              </span><span class="s2">"width"</span><span class="p">:</span><span class="w"> </span><span class="mi">800</span><span class="p">,</span><span class="w">
              </span><span class="s2">"height"</span><span class="p">:</span><span class="w"> </span><span class="mi">251</span><span class="w">
            </span><span class="p">}</span><span class="w">
          </span><span class="p">],</span><span class="w">
          </span><span class="s2">"userId"</span><span class="p">:</span><span class="w"> </span><span class="s2">"1"</span><span class="p">,</span><span class="w">
          </span><span class="s2">"tokenId"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="w">
        </span><span class="p">},</span><span class="w">
        </span><span class="s2">"projectId"</span><span class="p">:</span><span class="w"> </span><span class="s2">"1469"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"userId"</span><span class="p">:</span><span class="w"> </span><span class="s2">"1"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"tokenId"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
        </span><span class="s2">"userStatusId"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
        </span><span class="s2">"userViewPdfId"</span><span class="p">:</span><span class="w"> </span><span class="s2">""</span><span class="p">,</span><span class="w">
        </span><span class="s2">"generic_risks"</span><span class="p">:</span><span class="w"> </span><span class="p">[</span><span class="w">
          </span><span class="p">{</span><span class="w">
            </span><span class="s2">"id"</span><span class="p">:</span><span class="w"> </span><span class="mi">11</span><span class="p">,</span><span class="w">
            </span><span class="s2">"title"</span><span class="p">:</span><span class="w"> </span><span class="s2">"CACO8 / Test Risk Assessment"</span><span class="p">,</span><span class="w">
            </span><span class="s2">"ra_number"</span><span class="p">:</span><span class="w"> </span><span class="s2">"CACO8"</span><span class="p">,</span><span class="w">
            </span><span class="s2">"url"</span><span class="p">:</span><span class="w"> </span><span class="s2">"https://demo.re-flow.co.uk/uploads/risk_assessments/2017/SHE-02-Dynamic-Specific-Risk-Assessment.pdf"</span><span class="w">
          </span><span class="p">},</span><span class="w">
          </span><span class="p">{</span><span class="w">
            </span><span class="s2">"id"</span><span class="p">:</span><span class="w"> </span><span class="mi">12</span><span class="p">,</span><span class="w">
            </span><span class="s2">"title"</span><span class="p">:</span><span class="w"> </span><span class="s2">"CACO3 / Test Risk Assessment 2"</span><span class="p">,</span><span class="w">
            </span><span class="s2">"ra_number"</span><span class="p">:</span><span class="w"> </span><span class="s2">"CACO3"</span><span class="p">,</span><span class="w">
            </span><span class="s2">"url"</span><span class="p">:</span><span class="w"> </span><span class="s2">"https://demo.re-flow.co.uk/uploads/risk_assessments/2017/test.pdf"</span><span class="w">
          </span><span class="p">}</span><span class="w">
        </span><span class="p">],</span><span class="w">
        </span><span class="s2">"other_risks"</span><span class="p">:</span><span class="w"> </span><span class="p">[</span><span class="w">
          </span><span class="p">{</span><span class="w">
            </span><span class="s2">"id"</span><span class="p">:</span><span class="w"> </span><span class="mi">5</span><span class="p">,</span><span class="w">
            </span><span class="s2">"title"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Test Risk Assessment 2"</span><span class="p">,</span><span class="w">
            </span><span class="s2">"ra_number"</span><span class="p">:</span><span class="w"> </span><span class="s2">"CACO3"</span><span class="p">,</span><span class="w">
            </span><span class="s2">"url"</span><span class="p">:</span><span class="w"> </span><span class="s2">"https://demo.re-flow.co.uk/uploads/risk_assessments/2017/test.pdf"</span><span class="p">,</span><span class="w">
            </span><span class="s2">"assessmentId"</span><span class="p">:</span><span class="w"> </span><span class="s2">"1396"</span><span class="w">
          </span><span class="p">},</span><span class="w">
          </span><span class="p">{</span><span class="w">
            </span><span class="s2">"id"</span><span class="p">:</span><span class="w"> </span><span class="mi">6</span><span class="p">,</span><span class="w">
            </span><span class="s2">"title"</span><span class="p">:</span><span class="w"> </span><span class="s2">"Test Risk Assessment"</span><span class="p">,</span><span class="w">
            </span><span class="s2">"ra_number"</span><span class="p">:</span><span class="w"> </span><span class="s2">"CACO8"</span><span class="p">,</span><span class="w">
            </span><span class="s2">"url"</span><span class="p">:</span><span class="w"> </span><span class="s2">"https://demo.re-flow.co.uk/uploads/risk_assessments/2017/SHE-02-Dynamic-Specific-Risk-Assessment.pdf"</span><span class="p">,</span><span class="w">
            </span><span class="s2">"assessmentId"</span><span class="p">:</span><span class="w"> </span><span class="s2">"1390"</span><span class="w">
          </span><span class="p">}</span><span class="w">
        </span><span class="p">],</span><span class="w">
        </span><span class="s2">"dateUpdated"</span><span class="p">:</span><span class="w"> </span><span class="s2">"2017-03-28 16:33:41"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"dateCreated"</span><span class="p">:</span><span class="w"> </span><span class="s2">"2017-03-28 16:33:41"</span><span class="p">,</span><span class="w">
        </span><span class="s2">"isDraft"</span><span class="p">:</span><span class="w"> </span><span class="kc">false</span><span class="w">
      </span><span class="p">}</span><span class="w">
    </span><span class="p">]</span><span class="w">
  </span><span class="p">}</span><span class="w">
</span><span class="p">}</span><span class="w">
</span></code></pre>
      <h3 id="http-request">HTTP Request</h3>

      <p><code class="prettyprint">GET /v1/api/&lt;formName&gt;</code></p>

      <h3 id="header-query-parameters">Header / Query Parameters</h3>

      <p>The following parameters can be set as headers or be written as part of the GET string</p>

      <table><thead>
        <tr>
          <th>Parameter</th>
          <th>Default</th>
          <th>Description</th>
        </tr>
        </thead><tbody>
        <tr>
          <td>limit</td>
          <td>10</td>
          <td>The maximum number of items to return</td>
        </tr>
        <tr>
          <td>offset</td>
          <td>0</td>
          <td>The number of items to skip</td>
        </tr>
        </tbody></table>

      <h3 id="content-searching">Content Searching</h3>

      <p>It is possible to search for specific field values with more advanced logic.</p>

      <p>Prefixing field values with a key will specify the search type. Fields values without a prefix, will default to eq:.</p>

      <table><thead>
        <tr>
          <th>Prefix</th>
          <th>Description</th>
        </tr>
        </thead><tbody>
        <tr>
          <td>eq:</td>
          <td>Equals</td>
        </tr>
        <tr>
          <td>lt:</td>
          <td>Less Than</td>
        </tr>
        <tr>
          <td>lte:</td>
          <td>Less Than or Equal To</td>
        </tr>
        <tr>
          <td>gt:</td>
          <td>Greater Than</td>
        </tr>
        <tr>
          <td>gte:</td>
          <td>Greater Than or Equal To</td>
        </tr>
        </tbody></table>

      <p>It is also possible to combine 2 queries on 1 field using by adding <code class="prettyprint">AND</code> or <code class="prettyprint">OR</code> in-between the two values you wish to use. For example, searching between two dates for a field called &lsquo;dateUpdated&rsquo; would be <code class="prettyprint">dateUpdated=gte:2017-01-01T00:00:00 AND lte:2018-01-01T00:00:00</code>.</p>

      <h3 id="content-search-parameters">Content Search Parameters</h3>

      <p>The following parameters are searchable with the logic above. They are specified in the GET string.</p>

      <table><thead>
        <tr>
          <th>Parameter</th>
          <th>Description</th>
        </tr>
        </thead><tbody>
        <tr>
          <td>projectId</td>
          <td>The id of the job</td>
        </tr>
        <tr>
          <td>userId</td>
          <td>The id of the user who submitted the form</td>
        </tr>
        <tr>
          <td>dateCreated</td>
          <td>When the form was submitted to the system (UTC)</td>
        </tr>
        <tr>
          <td>dateUpdated</td>
          <td>When the form was last updated in the system (UTC)</td>
        </tr>
        </tbody></table>

      <h3 id="returned-data">Returned Data</h3>

      <p>The data structure / fields returned vary depending on the type of form requested. All forms have the following metadata</p>

      <table><thead>
        <tr>
          <th>Parameter</th>
          <th>Description</th>
        </tr>
        </thead><tbody>
        <tr>
          <td>id</td>
          <td>The id of the form</td>
        </tr>
        <tr>
          <td>formStatus</td>
          <td>&gt; 0 means the form has been submitted and can no longer be edited in the app</td>
        </tr>
        <tr>
          <td>formName</td>
          <td>The name of the form instance</td>
        </tr>
        <tr>
          <td>userId</td>
          <td>The id of the user who submitted the form</td>
        </tr>
        <tr>
          <td>dateCreated</td>
          <td>When the form was submitted to the system (UTC)</td>
        </tr>
        <tr>
          <td>dateUpdated</td>
          <td>When the form was last updated in the system (UTC)</td>
        </tr>
        <tr>
          <td>transactionId</td>
          <td>A unique id generated by the app</td>
        </tr>
        </tbody></table>

    </div>
    <div class="dark-box">
    </div>
  </div>
<? } else { ?>
  <form action="/apidocs/index.php" method="post" style="padding: 20px 40px" >
    <h2>Please enter the password</h2>
    Password <input type="password" name="access">
    <input type="submit" name="submit">
  </form>

<? } ?>
</body>
</html>

