var gulp		= require('gulp'),
	rename		= require('gulp-rename'),
	uglify		= require('gulp-uglify'),
	minifyCSS	= require('gulp-minify-css'),
	SASS		= require('gulp-sass'),
	sourcemaps	= require('gulp-sourcemaps'),
	prefix		= require('gulp-autoprefixer'),
	watch		= require('gulp-watch'),
	imagemin	= require('gulp-imagemin'),
	notify		= require("gulp-notify"),
	changed		= require('gulp-changed'),
	ftp			= require( 'vinyl-ftp' );

gulp.task('sass', function() {

	gulp.src(['assets/css/*.css', '!assets/css/*.min.css'])
		.pipe(minifyCSS())
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest('assets/css'));

	gulp.src('src/sass/style.scss')
		.pipe(changed('assets/css', {hasChanged: changed.compareContents}))
		.pipe(sourcemaps.init())
		.pipe(SASS().on('error', function(err) { return notify({
				title: 'Sass Error <%= error.relativePath %>',
				subtitle: 'line: <%= error.line %>',
				message: "message: <%= error.message %>",
			}).write(err);
		}))
		.pipe(prefix('last 5 versions'))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('assets/css'));
});

gulp.task('imagemin', function () {

	gulp.src('src/images/*')
		.pipe(changed('assets/images', {hasChanged: changed.compareContents}))
		.pipe(imagemin([
			imagemin.gifsicle({interlaced: true}),
			imagemin.jpegtran({progressive: true}),
			imagemin.optipng({optimizationLevel: 5}),
			imagemin.svgo({plugins: [{removeViewBox: true}]})
		]))
		.pipe(gulp.dest('assets/images'))
});

gulp.task('watch', function() {
	gulp.watch('src/images/*', ['imagemin']);
	gulp.watch('src/sass/*.scss', ['sass']);
});

gulp.task('default', ['sass']);
