<?php

/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/general.php
 */

return array(

    '*' => array(
        //
    ),

    're-flow.co.uk' => array(
        'devMode' => false,
    ),

    're-flow.local' => array(
        'siteUrl' => 'http://re-flow.local',
        'devMode' => true,
    )


);
