<?php

/**
 * Database Configuration
 *
 * All of your system's database configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/db.php
 */

return array(

    '*' => array(),

    're-flow.co.uk' => array(

        // The database server name or IP address. Usually this is 'localhost' or '127.0.0.1'.
        'server' => 'localhost',

        // The database username to connect with.
        'user' => 'reflowco_data',

        // The database password to connect with.
        'password' => 'W;+(.gfhT0qD',

        // The name of the database to select.
        'database' => 'reflowco_new_data',

        // The prefix to use when naming tables. This can be no more than 5 characters.
        'tablePrefix' => 'craft',

    ),

    're-flow.local' => array(

        // The database server name or IP address. Usually this is 'localhost' or '127.0.0.1'.
        'server' => 'localhost',

        // The database username to connect with.
        'user' => 'root',

        // The database password to connect with.
        'password' => '',

        // The name of the database to select.
        'database' => 'reflowco_new_data',

        // The prefix to use when naming tables. This can be no more than 5 characters.
        'tablePrefix' => 'craft',

    ),

);